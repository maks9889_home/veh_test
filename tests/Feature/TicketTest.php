<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class TicketTest extends TestCase
{
//    use RefreshDatabase;
    use DatabaseTransactions;

    /**
     * A basic functional test post '/tickets'.
     *
     * @return void
     */
    public function testPostTicket()
    {
        $response = $this->json('POST', '/tickets');

        $response->assertStatus(200);

        $ticketId = $response->decodeResponseJson();

        self::assertIsNumeric($ticketId);
    }

    /**
     * A basic functional test post 'payments/{ticket}'
     *
     * @return void
     */
    public function testPostTicketPayment()
    {
        $response = $this->json('POST', '/tickets');
        $ticketId = $response->decodeResponseJson();

        $response = $this->json('POST', '/payments/' . $ticketId, ['card_number' => '32435']);

        $response->assertStatus(200);

        $content = $response->decodeResponseJson();
        self::assertIsArray($content['payment']);
    }
}
