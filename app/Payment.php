<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    /**
     * {@inheritdoc}
     */
    protected $fillable = ['card_number_secured', 'amount', 'rate_id', 'ticket_id'];
}
