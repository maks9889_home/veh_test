<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ParkingLot extends Model
{
    /**
     * {@inheritdoc}
     */
    public $timestamps = false;

    /**
     * {@inheritdoc}
     */
    protected $fillable = ['lot_name'];

    /**
     * Return active ticket for the Lot
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ticket()
    {
        $instance = $this->hasMany('App\Ticket');
        $instance->where('paid', '=', false);
        return $instance;
    }
}
