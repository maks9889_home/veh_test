<?php

namespace App\Http\Controllers;

use App\ParkingLot;
use App\Ticket;
use Illuminate\Http\Request;

class TicketsController extends Controller
{
    public function __construct()
    {
        //Todo: uncomment for real API
        //$this->middleware('auth:api')->except(['index', 'show']);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function index()
    {
        return Ticket::with('parkingLot')->get();
    }

    /**
     * @param $id
     * @return Ticket
     */
    public function show($id)
    {
        // Todo: add data about paymens and Parking lot to response
        return Ticket::find($id);
    }

    /**
     * @param Request $request
     * @return array|int
     */
    public function store(Request $request)
    {
        $lots = ParkingLot::all();
        $lot = $lots->first(function ($lot) {
            return $lot->ticket->isEmpty();
        });

        if (null == $lot) {
            return ['errors' => [
                'errors' => [
                    'free-lot-error' => 'We can\'t find free lot for you . Please try again later'
                ]
            ]];
        }

        $ticket = new Ticket();
        $ticket->parking_lot_id = $lot->id;
        $ticket->save();


        return $ticket->id;
    }

    /**
     * @param $id
     * @param Request $request
     * @return array|Ticket
     */
    public function pay($id, Request $request)
    {
        $request->validate([
            'card_number' => 'required|numeric',
        ]);

        try {
            $ticket = Ticket::findOrFail($id);

            //Todo: add real payments process here
            $isCardVerified = true;

            if ($isCardVerified && !$ticket->paid) {
                $ticket->calculateAndPayRate($request->card_number);

                // return current ticket with all payments inside
                return Ticket::with('payment')->whereIn('id', [$id])->first();
            } else {
                if ($ticket->paid) {
                    throw new \Exception('Ticket was already paid');
                } else {
                    throw new \Exception('Card verification problem');
                }
            }
        } catch (\Exception $exception) {
            return ['errors' => [
                'errors' => [
                    'pay-error' => $exception->getMessage()
                ]
            ]];
        }

        return ['message' => 'Ticket has been sent successfully!'];
    }
}
