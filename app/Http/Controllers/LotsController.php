<?php

namespace App\Http\Controllers;

use App\ParkingLot;

class LotsController extends Controller
{
    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function index()
    {
        return ParkingLot::with('ticket')->get();
    }

    /**
     * @param $id
     * @return ParkingLot
     */
    public function show($id)
    {
        return ParkingLot::find($id);
    }
}
