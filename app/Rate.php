<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    /**
     * {@inheritdoc}
     */
    protected $fillable = ['name', 'amount', 'hours'];
}
