<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    /**
     * @param int $card_number
     */
    public function calculateAndPayRate($card_number)
    {
        $hours = $this->created_at->diffInHours() + 0.001;
        $rates = Rate::orderBy('hours')->get();

        while ($hours > 0) {
            foreach ($rates as $rate) {
                if ($rate->hours > $hours) {
                    break;
                }
            }

            Payment::create([
                'card_number_secured' => md5($card_number), // unsafe save real card number so we save only hash fro test/history
                'amount' => $rate->amount,
                'rate_id' => $rate->id,
                'ticket_id' => $this->id
            ]);

            $hours -= $rate->hours;
        }

        $this->paid = true;
        $this->save();

    }

    public function payment()
    {
        return $this->hasMany('App\Payment');
    }

    public function parkingLot()
    {
        return $this->belongsTo('App\ParkingLot');
    }
}
