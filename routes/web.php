<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('test');
});

//Todo move routes below to routes/api.php for real api
Route::get('/lots', function (Request $request) {
    var_dump($request);
});

Route::get('lots', 'LotsController@index');
Route::get('lots/{id}', 'LotsController@show');

Route::get('tickets', 'TicketsController@index');
Route::get('tickets/{ticket}', 'TicketsController@show');
Route::post('tickets', 'TicketsController@store');
Route::post('payments/{ticket}', 'TicketsController@pay');
