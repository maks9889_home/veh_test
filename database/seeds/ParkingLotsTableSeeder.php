<?php

use Illuminate\Database\Seeder;

class ParkingLotsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rowArr = ['A', 'B'];
        $colArr = [1, 2];

        foreach ($rowArr as $row) {
            foreach ($colArr as $col) {
                App\ParkingLot::create(['lot_name' => $row . $col]);
            }
        }
    }
}
