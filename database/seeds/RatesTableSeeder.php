<?php

use Illuminate\Database\Seeder;

class RatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rate = App\Rate::create(['name' => '1h', 'hours' => 1, 'amount' => 3]);
        $rate = App\Rate::create(['name' => '3h', 'hours' => 3, 'amount' => $rate->amount * 1.5]);
        $rate = App\Rate::create(['name' => '6h', 'hours' => 6, 'amount' => $rate->amount * 1.5]);
        App\Rate::create(['name' => 'ALL DAY', 'hours' => 24, 'amount' => $rate->amount * 1.5]);
    }
}
